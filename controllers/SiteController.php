<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Alumnos;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionInsertar() {
        
        // creo un modelo para poderlo rellenar con los modelos del formulario
        // y almacenarlo en la tabla
        $model = new Alumnos();
        
        // compruebo si vengo del formulario
        if ($model->load(Yii::$app->request->post())) {
            // si puedo almacenar el registro le mando a la pagina de inicio
            if ($model->save()) {
                return $this->redirect(["site/index"]) ;
            }
        }
        
        return $this->render('formulario', [
            'model' => $model,
            'accion' => 'Añadir',
        ]);
    }
    
    public function actionUpdate($id) {
        
        // cargar desde la base de datos el alumno a modificar
        $model= Alumnos::findOne($id);
        
        // compruebo si vengo del formulario
        if($this->request->post()){        
            // cargo los datos del formulario actualizados en el modelo
            if ($model->load(Yii::$app->request->post())) {
                // instento almacenar los datos actualizados
                if ($model->save()) {
                    return $this->redirect(["site/index"]) ;
                }
            }
        }
        return $this->render('formulario', [
            'model' => $model,
            'accion' => 'Actualizar',
        ]);
    }
    
    public function actionEliminar($codigo) {
        // crago los datos del alumno a eliminar en el modelo
        $model= Alumnos::findOne($codigo);
        
        // si has pulsado el boton eliminar del formulario
        // elimino el registr
        if($this->request->isPost){
            
        
            // elimino el registro
            $model->delete();
            return $this->redirect(["site/index"]);
        }
        
        // sacar formulario de confirmar eliminacion
        return $this->render('formulario',[
            'model' => $model,
            'accion' => 'Eliminar',
        ]);
    }
    
    public function actionListarlist() {
        $dataProvider = new ActiveDataProvider([
            'query' => Alumnos::find(),
        ]);
        
        return $this->render('listarlist',[
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionListargrid() {
        
        // creo un objeto de tipo ActiveDataProvider
        // para mandarselo al gridview
        $dataProvider = new ActiveDataProvider([
            'query' => Alumnos::find(),
        ]);
        
        // llamo a la vista donde utilizare el gridview
        return $this->render('listargrid',[
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionModificargrid() {
        
        // creo un objeto de tipo ActiveDataProvider
        // para mandarselo al gridview
        $dataProvider = new ActiveDataProvider([
            'query' => Alumnos::find(),
        ]);
        
        // llamo a la vista donde utilizare el gridview
        return $this->render('modificargrid',[
            'dataProvider' => $dataProvider,
        ]);
    }
    
     public function actionEliminargrid() {
        
        // creo un objeto de tipo ActiveDataProvider
        // para mandarselo al gridview
        $dataProvider = new ActiveDataProvider([
            'query' => Alumnos::find(),
        ]);
        
        // llamo a la vista donde utilizare el gridview
        return $this->render('eliminargrid',[
            'dataProvider' => $dataProvider,
        ]);
    }
    
    
    
}
