<?php
use yii\grid\GridView;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'codigoAlumno',
        'nombre',
        'apellidos',
        'correo',
        'telefono',
        [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{update}',
        ]
    ]
]);